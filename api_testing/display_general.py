from flask import Flask, request
from flask import jsonify
import mysql.connector as database
app = Flask(__name__)

db = database.connect(
    host = 'net-plim-dev-02.netcom.duke.edu', 
    user = 'plim', 
    password='3WEe8jr5wvZe', 
    database = 'plim',
)
                        
cursor = db.cursor(dictionary=True)

database_tables = [
    'buildings',
    'rooms',
    'racks'
]

@app.route('/')
def default():
    table = request.args.get("table")
    if not table in database_tables:
        return jsonify({"error": "table does not exist"})
    query = f"SELECT * FROM {table}"
    cursor.execute(query)

    racks = []
    for row in cursor:
        racks.append(row)

    db.close()
    return jsonify(racks)

app.run()

