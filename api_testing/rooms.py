from flask import Flask, request, jsonify
import mysql.connector as database
import json
app = Flask(__name__)

db = database.connect(host='net-plim-dev-02.netcom.duke.edu',
                    user='plim',
                    password='3WEe8jr5wvZe',
                    database='plim')
                    
cursor = db.cursor(dictionary=True)

@app.route('/') 
def default():

    lst = []
    cursor.execute("SELECT * FROM rooms")
    tables = []
    for row in cursor:
        tables.append(row)

    return jsonify(tables)

@app.route('/id/')
def rooms_id():
    room_id = request.args.get('id')
    
    #might be prone to SQL inj
    cursor.execute("SELECT * FROM rooms WHERE id = %s", (room_id,))
    t = []
    for row in cursor:
        t.append(row)
    return jsonify(t)
app.run()

