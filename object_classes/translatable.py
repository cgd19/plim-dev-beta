import json

class Translatable:
    def populate_from_json(self, json_input):
        '''
        Populates self with json key value pairs
        
        Takes the keys from a json string and sets them as attributes of self populated with their corresponding value.
        
        Parameters:
            self (Translatable): Object to give attributes to
            json_input (str): Json string with key value pairs to assign
        
        Returns:
        void
        '''
        temp_dict = json.loads(json_input)
        for key, value in temp_dict.items():
            setattr(self, key, value)

    def to_json(self):
        '''Returns a json representation of self'''
        temp_dict = {}
        for a in dir(self):
            if not a.startswith('__') and not callable(getattr(self, a)):
                temp_dict[a] = getattr(self, a)
        return json.dumps(temp_dict)


class Dog(Translatable):
    def __init__(self, name = '', spots = 0, fav_foods = []):
        self.name = name
        self.spots = spots
        self.fav_foods = fav_foods

ricky = Dog('rickey', 32, ['pizaa', 'caviar'])

print(ricky.to_json())

sammy = Dog()
sammy.populate_from_json('{"fav_foods": ["pizaa", "bone"], "name": "sammy", "spots": 1}')

print(sammy.name)
print(sammy.to_json())