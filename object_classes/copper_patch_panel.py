class CopperPatchPanels():
    def __init__(self, id, rack_id, manufacturer, model, height, num_ports):
        self.id = id
        self.rack_id = rack_id
        self.manufacturer = manufacturer
        self.model = model
        self.height = height 
        self.num_ports = num_ports