def CopperCable():
    def __init__(self, connector_id, connections, category, cable_usage, port):
        self.connector_id = connector_id
        self.connections = connections
        self.category = category
        self.cable_usage = cable_usage
        self.port = port