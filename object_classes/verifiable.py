class Verifiable:
    verified = True
    verifiers = []

    def verify(self):
        for func in self.verifiers:
            temp_result = self.func()
            if not temp_result:
                self.verified = False
                break
    
