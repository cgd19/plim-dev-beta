class Fiber_cable:
    def __init__(self, id, connector_id, cable_usage, number_of_strands, connections, ports):
        self.id = id
        self.connector_id = connector_id
        self.cable_usage = cable_usage
        self.number_of_strands = number_of_strands
        self.connections = connections
        self.ports = ports

