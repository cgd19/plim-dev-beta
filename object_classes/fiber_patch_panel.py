class Fiber_patch_panel:
    def __init__(self, id, shelf_id, position, manufacturer, model, strand_count, termination_types, polish):
        self.id = id
        self.shelf_id = shelf_id
        self.position = position
        self.manufacturer = manufacturer
        self.model = model
        self.strand_count = strand_count
        self.termination_types = termination_types
        self.polish = polish
