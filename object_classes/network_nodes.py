import ipaddress


class NetworkNodes: 
    def __init__(self, id, rack_id, management_ip, position, purpose, manufacturer, model, height, num_ports):
        self.id = id
        self.rack_id = rack_id
        if(ipaddress.ip_address(management_ip)):
            self.management_ip = management_ip
        self.position = position
        self.purpose = purpose
        self.manufacturer = manufacturer
        self.model = model
        self.height = height
        self.num_ports = num_ports
    

    #def verify_management_ip(self):
        #ipaddress.ip_address(self.management_ip)


#testInvalid = NetworkNodes(1,1,"19d2.1.17.2", 2, "Switch", "Cisco", "SCSCD", 8, 24)
#testValid = NetworkNodes(1,1,"192.1.17.2", 2, "Switch", "Cisco", "SCSCD", 8, 24)