from flask import Flask, request, jsonify
import mysql.connector as database
import json

app = Flask(__name__)

@app.route("/")
def index():
    return "<h1>Hello</h1>"


@app.route("/rooms")
def get_room():
    return {"room":"room b"}

class Rooms():
    def __init__(self, id,building_id, room_name, room_number, room_type, room_description):
        self.id = id
        self.building_id = building_id
        self.room_name = room_name
        self.room_number = room_number
        self.room_type = room_type
        self.room_description = room_description
    

app.run()