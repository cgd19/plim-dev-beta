class Buildings: 
    def __init__(self, id, name, number, location, description):
        self.id = id
        self.name = name
        self.number = number
        self.location = location
        self.description = description
    
    def displayId(self):
        print("The id of", self.name, "is", self.id)

    def displayNumber(self):
        print("The number of", self.name, "is", self.number)

    def displayLocation(self):
        print("The location of", self.name, "is", self.location)

    def displayDescription(self):
        print("The id of", self.name, "is", self.description)

bostock = Buildings(1, "Bostock Library", 14, "411 Chapel Dr", "main_library")
bostock.displayId()
bostock.displayLocation()