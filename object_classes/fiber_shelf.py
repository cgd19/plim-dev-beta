class Fiber_shelf:
    def __init__(self, id, rack_id, position, manufacturer, model, slots, height):
        self.id = id
        self.rack_id = rack_id
        self.position = position
        self.manufacturer = manufacturer
        self.model = model
        self.slots = slots
        self.height = height
