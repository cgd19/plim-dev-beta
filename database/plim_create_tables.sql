--File that contains table drops and creation with no imports

use plim;

drop table if exists buildings;
create table buildings (
    id                      int auto_increment primary key, 
    building_name           varchar(255), 
    building_number         int, 
    building_location       varchar(255), 
    building_description    text
);

drop table if exists rooms;
create table rooms (
    id                      int auto_increment primary key, 
    building_id             int, 
    room_name               varchar(255), 
    floor                   varchar(255), 
    room_number             varchar(255), 
    room_type               varchar(255), 
    room_description        text
);

drop table if exists rack_info;
create table rack_info(
    id                      int auto_increment primary key, 
    form_factor             varchar(255), 
    height                  int, 
    hole_type               varchar(255), 
    hole_grouping           int
);

drop table if exists racks;
create table racks(
    id                      int auto_increment primary key, 
    info_id                 int, 
    room_id                 int, 
    label                   varchar(255)
); 

drop table if exists fiber_shelf_info;
create table fiber_shelf_info (
    id                      int auto_increment primary key, 
    manufacturer            varchar(255), 
    model                   varchar(255), 
    slots                   int, 
    height                  int
);

drop table if exists fiber_shelves;
create table fiber_shelves (
    id                      int auto_increment primary key, 
    info_id                 int, 
    rack_id                 int, 
    position                int, 
    label                   varchar(255)
);

drop table if exists copper_patch_panel_info;
create table copper_patch_panel_info (
    id                      int auto_increment primary key, 
    manufacturer            varchar(255), 
    model                   varchar(255), 
    height                  int, 
    num_ports               int
);

drop table if exists copper_patch_panels;
create table copper_patch_panels (
    id                      int auto_increment primary key, 
    info_id                 int, 
    rack_id                 int, 
    position                int, 
    label                   varchar(255)
);

drop table if exists fiber_patch_panel_info;
create table fiber_patch_panel_info (
    id                      int auto_increment primary key, 
    manufacturer            varchar(255), 
    model                   varchar(255), 
    strand_count            int, 
    termination_types       varchar(255), 
    polish                  varchar(255)
);

drop table if exists fiber_patch_panels;
create table fiber_patch_panels (
    id                      int auto_increment primary key, 
    info_id                 int, 
    shelf_id                int, 
    position                varchar(255), 
    label                   varchar(255)
);

drop table if exists wall_plates;
create table wall_plates(
    id                      int auto_increment primary key, 
    room_id                 int, 
    total_ports             int, 
    used_ports              int, 
    label                   varchar(255)
);

drop table if exists copper_cable_info;
create table copper_cable_info(
    id                      int auto_increment primary key, 
    category                varchar(255), 
    cable_type              text
);

drop table if exists copper_cables;
create table copper_cables(
    id                      int auto_increment primary key, 
    info_id                 int, 
    conductor               int, 
    cable_usage             text, 
    wall_plate_id_1         int, 
    wall_plate_port_1       int, 
    copper_patch_panel_id_1 int, 
    copper_patch_panel_port_1 int, 
    copper_patch_panel_id_2 int, 
    copper_patch_panel_port_2 int, 
    network_node_id_1       int, 
    network_node_port_1     int, 
    network_node_id_2       int, 
    network_node_port_2     int, 
    waps_id_1               int, 
    waps_port_1             int
);

drop table if exists fiber_cable_info;
create table fiber_cable_info(
    id                      int auto_increment primary key, 
    cable_type              varchar(255), 
    number_of_strands       int
);

drop table if exists fiber_cables;
create table fiber_cables(
    id                      int auto_increment primary key, 
    info_id                 int, 
    connector_id            int, 
    cable_usage             text, 
    fiber_patch_panel_id_1  int, 
    fiber_patch_panel_port_1 int, 
    fiber_patch_panel_id_2  int, 
    fiber_patch_panel_port_2 int, 
    network_node_id_1       int, 
    network_node_port_1     int, 
    network_node_id_2       int, 
    network_node_port_2     int
);

drop table if exists fiber_connector_info;
create table fiber_connector_info(
    id                      int auto_increment primary key, 
    model                   text, 
    polish                  text
);

drop table if exists network_node_info;
create table network_node_info(
    id                      int auto_increment primary key, 
    purpose                 text, 
    manufacturer            text, 
    model                   text, 
    height                  int, 
    num_ports               int
);

drop table if exists network_nodes;
create table network_nodes (
    id                      int auto_increment primary key, 
    info_id                 int, 
    rack_id                 int, 
    management_ip           varchar(255), 
    position                int, 
    label                   varchar(255)
);

drop table if exists wap_info;
create table wap_info(
    id                      int auto_increment primary key, 
    manufacturer            text, 
    model                   text, 
    radios                  int, 
    num_ports               int
);

drop table if exists waps;
create table waps (
    id                      int auto_increment primary key, 
    info_id                 int, 
    room_id                 int, 
    management_ip           varchar(255), 
    x_pos                   float, 
    y_pos                   float, 
    image_path              varchar(255), 
    label                   varchar(255)
);
