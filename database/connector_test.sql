
drop table if exists n1;
create table n1 (id int auto_increment primary key, label varchar(255), number_of_ports int);

drop table if exists n2;
create table n2 (id int auto_increment primary key, label varchar(255), number_of_ports int);

drop table if exists n3;
create table n3 (id int auto_increment primary key, label varchar(255), number_of_ports int);

----CABLES: consolidates all cables into one table with generic columns to support connecting to any device
--id
--connection_id_1/2:    the id of the device connected to the wire
--connection_type_1/2:  the type of the device connected to the wire (used to select tables)
--connection_port_1/2:  the specific port of a device that a cable is connected to
--cable_usage:          the specific role that a cable plays, i.e. patch, horizontal, riser, etc.
--cable_type_id:        id of the row that contains information about the cable's type
drop table if exists cables;
create table cables (
    id                      int auto_increment primary key, 
    connection_id_1         int, 
    connection_type_1       varchar(255),
    connection_port_1       int,
    connection_id_2         int,
    connection_type_2       varchar(255),
    connection_port_2       int,
    cable_usage             varchar(255),
    cable_info_id           int
);

----CABLE INFO: table holds information about the different types of copper and fiber cables
--id
--material:     specifies copper/fiber
--cable_type:   type of cable, i.e. UTP/STP or OM1/OM3
--cores:        number of strands/conductors within a cable
--category:     category of copper cable (copper cables only)
drop table if exists cable_info;
create table cable_info (
    id                      int auto_increment primary key,
    material                varchar(255),
    cable_type              varchar(255),
    cores                   int,
    category                varchar(255)
)


insert into n1 (label, number_of_ports) values ('box1', 4);
insert into n1 (label, number_of_ports) values ('box2', 1);

insert into n2 (label, number_of_ports) values ('switch1', 16);
insert into n2 (label, number_of_ports) values ('switch2', 16);

insert into n3 (label, number_of_ports) values ('router1', 16);
insert into n3 (label, number_of_ports) values ('router2', 16);

insert into cables (
    connection_id_1, 
    connection_type_1, 
    connection_port_1, 
    connection_id_2, 
    connection_type_2, 
    connection_port_2,
    cable_usage,
    cable_type_id
) values (1, 'n1', 3, 2, 'n1', 1, 'horizontal', 1);

insert into cables (
    connection_id_1, 
    connection_type_1, 
    connection_port_1, 
    connection_id_2, 
    connection_type_2, 
    connection_port_2,
    cable_usage,
    cable_type_id
) values (1, 'n1', 1, 1, 'n2', 16, 'riser', 2);

insert into cable_info (material, cable_type, cores, category) values ('copper', 'UTP', 8, '6');
insert into cable_info (material, cable_type cores) values ('fiber', 'OM3', 8,);