----Conventions:
--id: primary key for table
--position: the numbered position of the bottom hole is 0
--info_id: this is the ID that links to the primary table's info table. e.g fiber_shelves (primary table) has 'info_id' column that linked to fiber_shelf_info (info table).
--room_id: this is usually a foreign key that links to a room present in the 'rooms' table.
--rack_id: this is usually a foreign key that links to a rack present in the 'racks' table.
--shelf_id: this is usually a foreign key that links to a rack present in the 'fiber_shelves' table.
--manufacturer: official vendor that made device
--model: manufacturing id from vendor.

use plim;

----BUILDINGS: This table models the buildings at Duke.
--id: see 'id' in conventions
--building_name: the name of the building.
--building_number: duke assigned building number.
--building_location: where the building is located
--building_description: main purpose of the building.
drop table if exists buildings;
create table buildings (id int auto_increment primary key, building_name varchar(255), building_number int, building_location varchar(255), building_description text);
insert into buildings(building_name, building_number, building_location, building_description) values ('Bostock Library', 14, '411 Chapel Dr', 'main library');
insert into buildings(building_name, building_number, building_location, building_description) values ('Kilgo Quad House P', 60, '104 Union Drive', 'dormitory');
insert into buildings(building_name, building_number, building_location, building_description) values ('Bryan University Center', 56, '125 Science Drive', 'student union');
insert into buildings(building_name, building_number, building_location, building_description) values ('LSRC', 78, '308 Research Drive', 'Levine Science research Center');

----ROOMS: This table models the rooms in the buildings.
--id: see 'id' in conventions
--room_name: the name of the room.
--floor: buidling floor that room is on.
--room_number: assigned room number.
--room_type: type of room (e.g classroom, equipment room)
--room_description: main purpose of the room.
--  adding floor as an attribute to the room table descriptive character
drop table if exists rooms;
create table rooms (id int auto_increment primary key, building_id int, room_name varchar(255), floor varchar(255), room_number varchar(255), room_type varchar(255), room_description text);
insert into rooms (building_id, room_name, room_number, room_type, room_description) values (4, 'Room B', '4226', '1','Classroom','Main Scope Room');
insert into rooms (building_id, room_name, room_number, room_type, room_description) values (4, 'Equipment Room A', '1', '4227', 'Equipment Room','Contains Equipment');
insert into rooms (building_id, room_name, room_number, room_type, room_description) values (3, 'Equipment Room C', '1', '1600', 'Equipment Room','Contains Equipment');
insert into rooms (building_id, room_name, room_number, room_type, room_description) values (3, 'Room D', 'D105', '1', 'Equipment Room','Contains Equipment');
insert into rooms (building_id, room_name, room_number, room_type, room_description) values (4, 'Equipment Room R', '2', '3200', 'Classroom','Main Scope Room');
insert into rooms (building_id, room_name, room_number, room_type, room_description) values (4, 'Room T', '2', '3207', 'Equipment Room','Contains Equipment');


----RACK INFO
--id: see 'id' in conventions
--form_factor: 2-post vs 4-post
--height: in terms of RU
--hole_type round or square
--hole_grouping (how many holes per RU (2/3))
drop table if exists rack_info;
create table rack_info(id int auto_increment primary key, form_factor varchar(255), height int, hole_type varchar(255), hole_grouping int);
insert into rack_info(form_factor, height, hole_type, hole_grouping) values ('2-post', 42, 'Round hole', 3);
insert into rack_info(form_factor, height, hole_type, hole_grouping) values ('not a rack', 0, 'Round hole', 0);


----RACKS
--id: see 'id' in conventions
--info_id: see 'info_id' in conventions
--room_id: see 'room_id' in conventions
--label: identifier for the object
drop table if exists racks;
create table racks(id int auto_increment primary key, info_id int, room_id int, label varchar(255)); 
insert into racks(info_id, room_id) values (1, 2);
insert into racks(info_id, room_id) values (1, 2);
insert into racks(info_id, room_id) values (1, 3);
insert into racks(info_id, room_id) values (1, 3);
insert into racks(info_id, room_id) values (1, 5);
insert into racks(info_id, room_id) values (1, 5);


----FIBER SHELF INFO
--id: see 'id' in conventions
--manufacturer: see 'manufacturer' in conventions
--model: see 'model' in conventions
--slots (no of panels in shelf)
--height: in RU
drop table if exists fiber_shelf_info;
create table fiber_shelf_info (id int auto_increment primary key, manufacturer varchar(255), model varchar(255), slots int, height int);
insert into fiber_shelf_info (manufacturer, model, slots, height) values ('corning', 'cch-04u', 12, 4);


----FIBER SHELVES
--id: see 'id' in conventions
--info_id: see 'info_id' in conventions
--rack_id: see 'rack_id' in conventions
--position (see convention)
--label: identifier for the object
drop table if exists fiber_shelves;
create table fiber_shelves (id int auto_increment primary key, info_id int, rack_id int, position int, label varchar(255));
insert into fiber_shelves (info_id, rack_id, position) values (1,2,0);
insert into fiber_shelves (info_id, rack_id, position) values (1,4,0);
insert into fiber_shelves (info_id, rack_id, position) values (1,2,5);


----COPPER PATCH PANEL INFO
--id: see 'id' in conventions
--manufacturer: see 'manufacturer' in conventions
--model: see 'model' in conventions
--height: in RU
--num_ports: how many ports present
drop table if exists copper_patch_panel_info;
create table copper_patch_panel_info (id int auto_increment primary key, manufacturer varchar(255), model varchar(255), height int, num_ports int);
insert into copper_patch_panel_info (manufacturer, model, height, num_ports) values ("Cable Express", "TKSH-69", 4, 48);

----COPPER PATCH PANELS
--id: see 'id' in conventions
--info_id: see 'info_id' in conventions
--rack_id: see 'rack_id' in conventions
--position (see convention)
--label: identifier for the object
drop table if exists copper_patch_panels;
create table copper_patch_panels (id int auto_increment primary key, info_id int, rack_id int, position int, label varchar(255));
insert into copper_patch_panels (info_id, rack_id, position) values (1, 1, 2);
insert into copper_patch_panels (info_id, rack_id, position) values (1, 3, 0);
insert into copper_patch_panels (info_id, rack_id, position) values (1, 3, 5);
insert into copper_patch_panels (info_id, rack_id, position) values (1, 6, 2);


----FIBER PATCH PANEL INFO
--id: see 'id' in conventions
--manufacturer: see 'manufacturer' in conventions
--model: see 'model' in conventions
--strand_count: number of strands on patch panel
--termination_types: termination type of patch panel (e.g LC, SC etc)
--polish: polish of patch panel (e.g UPC/APC)
drop table if exists fiber_patch_panel_info;
create table fiber_patch_panel_info (id int auto_increment primary key, manufacturer varchar(255), model varchar(255), strand_count int, termination_types varchar(255), polish varchar(255));
insert into fiber_patch_panel_info (manufacturer, model, strand_count, termination_types, polish) values ("Cable Express", "M2-0X8", 24, "LC", "UPC");
insert into fiber_patch_panel_info (manufacturer, model, strand_count, termination_types, polish) values ("GM", "GM-567", 24, "LC", "APC");

----FIBER PATCH PANELS
--id: see 'id' in conventions
--info_id: see 'info_id' in conventions
--shelf_id: see 'shelf_id' in conventions
--position: this 'position' is somewhat different from the one specified in the conventions, it stores the location of the fiber patch panel in the shelf.
--label: identifier for the object
drop table if exists fiber_patch_panels;
create table fiber_patch_panels (id int auto_increment primary key, info_id int, shelf_id int, position varchar(255), label varchar(255));
insert into fiber_patch_panels (info_id, shelf_id, position) values (1, 1, '5');
insert into fiber_patch_panels (info_id, shelf_id, position) values (2,2,'8');
insert into fiber_patch_panels (info_id, shelf_id, position) values (2,3,'6');

----WALL PLATES
--id: see 'id' in conventions
--room_id: see 'room_id' in conventions
--total_ports: total no of ports (open and closed)
--used_ports: total no of open ports that cables can connect to 
--label: identifier for the object

drop table if exists wall_plates;
create table wall_plates(id int auto_increment primary key, room_id int, total_ports int, used_ports int, label varchar(255));
insert into wall_plates(room_id, label, total_ports, used_ports) values (1, "Equipment Room A/ROOM B", 2, 2);
insert into wall_plates(room_id, label, total_ports, used_ports) values (4, "Equipment Room C/ROOM D", 2, 2);
insert into wall_plates(room_id, label, total_ports, used_ports) values (4, "Equipment Room C/ROOM D", 3, 2);
insert into wall_plates(room_id, label, total_ports, used_ports) values (6, "Equipment Room R/ROOM T", 2, 2);


----COPPER CABLE INFO
--id: see 'id' in conventions
--category: catergory type of copper cable (e.g 3,5,5a,6,6a)
--cable_type (UTP, STP)
drop table if exists copper_cable_info;
create table copper_cable_info(id int auto_increment primary key, category varchar(255), cable_type text);
insert into copper_cable_info(category, cable_type) values ("3", "UTP");
insert into copper_cable_info(category, cable_type) values ("5", "UTP");
insert into copper_cable_info(category, cable_type) values ("5e", "UTP");
insert into copper_cable_info(category, cable_type) values ("6", "UTP");
insert into copper_cable_info(category, cable_type) values ("6", "STP");
insert into copper_cable_info(category, cable_type) values ("6a", "STP");

----COPPER CABLES
--id: see 'id' in conventions
--info_id: see 'info_id' in conventions
--conductors: number of conductors used in cable (4 conductors or 8 conductors)
--cable_usage: usage of cable (Can be riser, horizontal, or patch)
  ----ASK WILL IF WE CAN CONNECT PATCH_PANELS TO NETWORK NODES BTW FLOORS (AND HOW WE WOULD CLASSIFY THOSE)*****
  -- if riser/horizontal-cross-connect (between patch panels in telecom closets)
    --copper_patch_panel_id_1: this is to indicate where the beginning of the copper cable is connected to.
    --copper_patch_panel_port_1: the port number where it is connected to.
    --copper_patch_panel_id_2: this is to indicate where the end of the copper cable is connected to.
    --copper_patch_panel_port_2: the port number where it is connected to.
 
  --if horizontal (patch in telecommunication closet to wall plate)
    --copper_patch_panel_id_1: this is to indicate where the beginning of the copper cable is connected to.
    --copper_patch_panel_port_1: the port number where it is connected to.
    --wall_plate_id: this is to indicate the wall plate it is connected to 
    --wall_plate_port: position on wall plate from label 
 
  --if patch_type_1 (network_node to copper_patch_panel)
    --copper_patch_panel_id_1: this is to indicate where the beginning of the copper cable is connected to.
    --copper_patch_panel_port_1: the port number where it is connected to.
    --network_node_id_1: the first network node the cable is connected to
    --network_node_port_1: the port number where it is connected to
 
  -- if patch_type_2 (network_node to network_node)
    --network_node_id_1: the first switch the cable is connected to
    --network_node_port_1: the port number where it is connected to
    --network_node_id_2: the second switch the cable is connected to 
    --network_node_port_2: the port number where it is connected to
  
  --if patch_type_3 (wall plate --> WAPS)
    --wall_plate_id: this is to indicate the wall plate it is connected to 
    --wall_plate_port: position on wall plate from label
    --waps_id: ID of the wireless access point
    --waps_port: port of connection of the wireless access point
  
  -- if unclassified (WAPs --> copper_patch_panel):
    --waps_id: ID of the wireless access point
    --waps_port: port of connection of the wireless access point
    --copper_patch_panel_id_1: this is to indicate where the beginning of the copper cable is connected to.
    --copper_patch_panel_port_1: the port number where it is connected to.


drop table if exists copper_cables;
create table copper_cables(id int auto_increment primary key, info_id int, conductor int, cable_usage text, wall_plate_id_1 int, wall_plate_port_1 int, copper_patch_panel_id_1 int, copper_patch_panel_port_1 int, copper_patch_panel_id_2 int, copper_patch_panel_port_2 int, network_node_id_1 int, network_node_port_1 int, network_node_id_2 int, network_node_port_2 int, waps_id_1 int, waps_port_1 int);
insert into copper_cables(info_id, conductor, cable_usage, wall_plate_id_1, wall_plate_port_1, copper_patch_panel_id_1, copper_patch_panel_port_1) values (1, 8, "Horizontal", 1, 1, 1, 10);
insert into copper_cables(info_id, conductor, cable_usage, wall_plate_id_1, wall_plate_port_1, copper_patch_panel_id_1, copper_patch_panel_port_1) values (6, 8, "Horizontal", 1, 2, 1, 12);
insert into copper_cables(info_id, conductor, cable_usage, copper_patch_panel_id_1, copper_patch_panel_port_1, network_node_id_1 , network_node_port_1 ) values (2, 8, "patch_type_1", 1, 2, 1, 12);
insert into copper_cables(info_id, conductor, cable_usage, waps_id_1, waps_port_1, copper_patch_panel_id_1, copper_patch_panel_port_1) values (1, 8, "unclassified",2,1, 1, 11); 
insert into copper_cables(info_id, conductor, cable_usage, copper_patch_panel_id_1, copper_patch_panel_port_1, copper_patch_panel_id_2, copper_patch_panel_port_2) values (5, 8, "riser", 1, 3,4,1);
insert into copper_cables(info_id, conductor, cable_usage, wall_plate_id_1, wall_plate_port_1, copper_patch_panel_id_1, copper_patch_panel_port_1) values (2, 8, "Horizontal", 2, 1, 2, 1);
insert into copper_cables(info_id, conductor, cable_usage, wall_plate_id_1, wall_plate_port_1, copper_patch_panel_id_1, copper_patch_panel_port_1) values (2, 8, "Horizontal", 2, 2, 2, 2);
insert into copper_cables(info_id, conductor, cable_usage, wall_plate_id_1, wall_plate_port_1, copper_patch_panel_id_1, copper_patch_panel_port_1) values (1, 8, "Horizontal", 3, 3, 3, 1);
insert into copper_cables(info_id, conductor, cable_usage, wall_plate_id_1, wall_plate_port_1, copper_patch_panel_id_1, copper_patch_panel_port_1) values (2, 8, "Horizontal", 3, 1, 3, 2);
insert into copper_cables(info_id, conductor, cable_usage, copper_patch_panel_id_1, copper_patch_panel_port_1, network_node_id_1 , network_node_port_1 ) values (2, 8, "patch_type_1", 3, 3, 3, 12);
insert into copper_cables(info_id, conductor, cable_usage, network_node_id_1 , network_node_port_1, network_node_id_2 , network_node_port_2 ) values (2, 8, "patch_type_2", 3, 11, 4, 2);
insert into copper_cables(info_id, conductor, cable_usage, copper_patch_panel_id_1, copper_patch_panel_port_1, copper_patch_panel_id_2, copper_patch_panel_port_2) values (4, 8, "horizontal-cross-connect", 2, 17,3,19);
insert into copper_cables(info_id, conductor, cable_usage, copper_patch_panel_id_1, copper_patch_panel_port_1, network_node_id_1 , network_node_port_1 ) values (5, 8, "patch_type_1", 4, 15, 5, 12);
insert into copper_cables(info_id, conductor, cable_usage, wall_plate_id_1, wall_plate_port_1, copper_patch_panel_id_1, copper_patch_panel_port_1) values (5, 8, "Horizontal", 4, 1, 4, 21);
insert into copper_cables(info_id, conductor, cable_usage, wall_plate_id_1, wall_plate_port_1, copper_patch_panel_id_1, copper_patch_panel_port_1) values (5, 8, "Horizontal", 4, 2, 4, 27);
insert into copper_cables(info_id, conductor, cable_usage, wall_plate_id_1 ,wall_plate_port_1, waps_id_1, waps_port_1 ) values (2, 8, "patch_type_3", 1, 1, 1, 1);
--insert into copper_cables(info_id, conductor, cable_usage, waps_id_1, waps_port_1,copper_patch_panel_id_1, copper_patch_panel_port_1) values (1, 8, "patch_type_4", 2, 5, 1, 7);



 
----FIBER CABLE INFO        
  --id: see 'id' in conventions
  --cable_type: type of cable (OM3, OS1, etc.)
  --number_of_strands: Number of strands of fiber cable
drop table if exists fiber_cable_info;
create table fiber_cable_info(id int auto_increment primary key, cable_type varchar(255), number_of_strands int);
insert into fiber_cable_info(cable_type, number_of_strands) values ("OM3", 2);
 
----FIBER CABLES
  --id: see 'id' in conventions
  --info_id: see 'info_id' in conventions
  --connector_id: corresponds with id from fiber connector info
  --cable_usage: usage of cable (Can be riser, patch, horizontal, or outdoor)
 
 --if riser (between floors)
  --fiber_patch_panel_id_1: this is to indicate where the beginning of the fiber cable is connected to.
  --fiber_patch_panel_port_1: the port number where it is connected to.
  --fiber_patch_panel_id_2: this is to indicate where the end of the fiber cable is connected to.
  --fiber_patch_panel_port_2: the port number where it is connected to.
 
 --if patch_type_1 (fiber patch panel --> fiber patch panel) -- mostly in the same equipment room
  --fiber_patch_panel_id_1: this is to indicate where the beginning of the fiber cable is connected to.
  --fiber_patch_panel_port_1: the port number where it is connected to.
  --fiber_patch_panel_id_2: this is to indicate where the end of the fiber cable is connected to.
  --fiber_patch_panel_port_2: the port number where it is connected to.
 
 --if patch_type_2 (fiber patch panel --> network node) -- mostly in the same equipment room
  --fiber_patch_panel_id_1: this is to indicate where the beginning of the fiber cable is connected to.
  --fiber_patch_panel_port_1: the port number where it is connected to.
  --network_node_id_1: the network node being connected to from a telcom closet
  --network_node_port_1: the port number where it is connected to
 
 --if patch_type_3 (network node --> network node) -- mostly in the same equipment room
  --network_node_id_1: the first network being connected [beginning]
  --network_node_port_1: the port number where it is connected to
  --network_node_id_2: the second network being connected [end]
  --network_node_port_2: the port number where it is connected to
 
 --if horizontal (from telecommunication closets to equipment -- mostly on same floor across rooms)
  --fiber_patch_panel_id_1: this is to indicate where the beginning of the fiber cable is connected to.
  --fiber_patch_panel_port_1: the port number where it is connected to.
  --network_node_id_1: the network node being connected to from a telcom closet
  --network_node_port_1: the port number where it is connected to.
 
 --if outdoor (between buildings) 
  --fiber_patch_panel_id_1: this is to indicate where the beginning of the fiber cable is connected to.
  --fiber_patch_panel_port_1: the port number where it is connected to.
  --fiber_patch_panel_id_2: this is to indicate where the end of the fiber cable is connected to.
  --fiber_patch_panel_port_2: the port number where it is connected to.
 
drop table if exists fiber_cables;
create table fiber_cables(id int auto_increment primary key, info_id int, connector_id int, cable_usage text, fiber_patch_panel_id_1 int, fiber_patch_panel_port_1 int, fiber_patch_panel_id_2 int, fiber_patch_panel_port_2 int, network_node_id_1 int, network_node_port_1 int, network_node_id_2 int, network_node_port_2 int);
insert into fiber_cables(info_id, connector_id, cable_usage, fiber_patch_panel_id_1, fiber_patch_panel_port_1, fiber_patch_panel_id_2, fiber_patch_panel_port_2) values (1, 1, "outdoor", 1, 10, 2, 16);
insert into fiber_cables(info_id, connector_id, cable_usage, fiber_patch_panel_id_1, fiber_patch_panel_port_1, fiber_patch_panel_id_2, fiber_patch_panel_port_2) values (1, 1, "patch_type_1", 1, 10, 3, 16);
insert into fiber_cables(info_id, connector_id, cable_usage, network_node_id_1, network_node_port_1,network_node_id_2,network_node_port_2 ) values (1, 1, "patch_type_3", 1, 7, 2, 19);
insert into fiber_cables(info_id, connector_id, cable_usage, fiber_patch_panel_id_1, fiber_patch_panel_port_1, network_node_id_1,network_node_port_1 ) values (1, 1, "patch_type_2", 1, 23, 1, 14);


----FIBER CONNECTOR INFO
--id: see 'id' in conventions
--model: The model of the fiber connector (Can be SC,LC, etc.)
--polish: The type of the polish of the fiber connector (Can be APC or UPC)
drop table if exists fiber_connector_info;
create table fiber_connector_info(id int auto_increment primary key, model text, polish text);
insert into fiber_connector_info(model, polish) values ("LC", "APC");

----NETWORK NODE INFO
--id: see 'id' in conventions
--purpose: what type of device (e.g router, switch, firewall)
--manufacturer: who manufactured the network ndoe
--model: the model of the network node
--height: height the network_node occupies in rack in RU
--num_ports: the number of ports the network_node has 
drop table if exists network_node_info;
create table network_node_info(id int auto_increment primary key, purpose text, manufacturer text, model text, height int, num_ports int);
insert into network_node_info(purpose, manufacturer, model, height, num_ports) values ("Switch", "Cisco", "CX-90", 5, 16);
insert into network_node_info(purpose, manufacturer, model, height, num_ports) values ("Router", "ISP", "IS-76", 3, 16);


----NETWORK NODES
--see 'id' in conventions
--info_id: see 'info_id' in conventions
--rack_id: corresponds with the id from the Rack table
--management_ip: duke assigned IP address 
--position: position on rack (see conventions)
--label: identifier for the object
drop table if exists network_nodes;
create table network_nodes (id int auto_increment primary key, info_id int, rack_id int, management_ip varchar(255), position int, label varchar(255));
insert into network_nodes(info_id, rack_id, management_ip, position) values (1, 2, "test switch 1", 20);
insert into network_nodes(info_id, rack_id, management_ip, position) values (1, 2, "test switch 2", 26);
insert into network_nodes(info_id, rack_id, management_ip, position) values (1, 4, "test switch 3", 6);
insert into network_nodes(info_id, rack_id, management_ip, position) values (2, 4, "test router 1", 12);
insert into network_nodes(info_id, rack_id, management_ip, position) values (1, 5, "test switch 4", 10);

----WAP (Wireless Acess Point) INFO
--id: see 'id' in conventions
--manufacturer: who manufactured the Wireless Access Point
--model: the model of the Wireless Access Point
--radios: number of radios the Wireless Access Point has
--num_ports: the number of ports the Wireless Access Point has 
drop table if exists wap_info;
create table wap_info(id int auto_increment primary key, manufacturer text, model text, radios int, num_ports int);
insert into wap_info(manufacturer, model, radios, num_ports) values ("microsoft", "ms-2020", 189, 16);
 
----WAPs
--id: see 'id' in conventions
--info_id: see 'info_id' in conventions
--room_id: see 'room_id' in conventions
--management_ip: duke assigned IP address
--x_position: gives us x-coordinate of where Wireless Access Point is located on map
--y_position: gives us y-coordinate of where Wireless Access Point is located on map
--image_path: folder path to 2-D map image of where Wireless Access Point is located
--label: identifier for the object
drop table if exists waps;
create table waps (id int auto_increment primary key, info_id int, room_id int, management_ip varchar(255), x_pos float, y_pos float, image_path varchar(255), label varchar(255));
insert into waps(info_id, room_id, management_ip, x_pos, y_pos, image_path) values (1, 1, "test wap 1", 3.091, 76.709, "users/desktop/floor_plan.jpg");
insert into waps(info_id, room_id, management_ip, x_pos, y_pos, image_path) values (1, 1, "test wap 2", 10.56, 45.89, "users/desktop/floor_plan.jpg");